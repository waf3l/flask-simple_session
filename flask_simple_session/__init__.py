# -*- coding: utf-8 -*-
"""
Flask extension for user login, logout and session manage.
"""
from flask import redirect, flash, session, url_for, make_response, current_app

from functools import wraps
from hashlib import sha512

import logging
import random
import string
import json

SIMPLE_CHARS = string.ascii_letters + string.digits


class SimpleSession(object):
    """
    Simple user session manager for flask app.

    After create the object we must call two methods:
    First call: register_model(db.Model) - this method register users db.Model is neccessery because we need to know
    the users table structure.

    Second call: register_redirect_url(flask.url_for url var) - we must know where to redirect the user
    when he is not logged in.

    """

    def __init__(self, logger_parent='app', app=None):
        """
        Initialize

        Args:
            logger_parent (str): parent for logger
            app (flask obj): flask app objec
        """
        self.logger = logging.getLogger('{0}.auth'.format(logger_parent))
        self.logger.info('init flask simple session')

        # declare var for flask app
        self.app = None

        # sql alchemy db instance
        self.DB_INSTANCE = None

        # user db.Model
        self.USER_MODEL = None

        # login attr
        self.LOGIN_ATTR = None

        # passwd attr
        self.PASSWD_ATTR = None

        # user id attr
        self.USER_ID_ATTR = None

        # user name attr
        self.USER_NAME_ATTR = None

        # flask.url_for
        self.REDIRECT_URL = None

        # flask session key for login check decorator
        self.LOGIN_KEY = None

        if app is not None:
            self.init_app(self.app)

    def init_app(self, app):
        """
        Init with factor app pattern

        Args:
            app (flask obj): flask app object
        """
        # assign flask app
        self.app = app

    def register_db_instance(self, db_instance):
        """
        Register sqlalchemy database instance

        Args:
            db_instance (sqlalchemy db instance): db instance

        """
        self.DB_INSTANCE = db_instance

    def register_user_model(self, user_model, user_id_attr, login_attr, passwd_attr, user_name_attr):
        """
        Register sqlalchemy user database model

        Args:
            user_model (db.Model): Flask-SQLAlchemy database model of users table
            user_id_attr (str): Name of id user attribute
            login_attr (str): Name of login attribute
            passwd_attr (str) Name of password attribute
            user_name_attr (str) Name of user name attribute
        """

        # user db model
        self.USER_MODEL = user_model

        # login attr
        self.LOGIN_ATTR = login_attr

        # passwd attr
        self.PASSWD_ATTR = passwd_attr

        # user id attr
        self.USER_ID_ATTR = user_id_attr

        # user name attr
        self.USER_NAME_ATTR = user_name_attr

    def register_redirect_url(self, url):
        """
        Register url data for url_for

        Args:
            url (flask.url_for url variable): url variable for flask.url_for
        """
        self.REDIRECT_URL = url

    def register_login_key(self, key):
        """
        Register session key for login check

        Args:
            key (str): name of key to check with login decorator
        """
        self.LOGIN_KEY = key

    def login(self, login, passwd):
        """
        Check login and password and when them are correct login the user


        Args:
            login (str): login to check
            passwd (str): password to check

        Returns:
            Tuple (Boolean,dict), If boolean is true the dict is None else the dict has structure:
                {'status':'warning','msg','warning message'} or
                {'status':'error','error_type':'Exception','msg':'error message'}
        """
        try:
            # user = self.USER.query.filter(self.USER.user_login == login).first()
            user = self.USER_MODEL.query.filter(getattr(self.USER_MODEL, self.LOGIN_ATTR) == login).first()

            # if login not found return warning msg
            if user is None:
                self.logger.warning('Wrong login: {0}'.format(login))
                return False, {'status': 'warning', 'msg': 'Wrong login or password'}

            # check password hash
            if user.check_password(passwd):
                # try to login user
                status, result = user.login()

                if status:
                    # user logged in set session
                    return self._set_session(user)
                else:
                    # user login error
                    return False, result
            else:
                # wrong password
                self.logger.warning('Wrong password: {0} for login: {1}'.format(passwd, login))
                return False, {'status': 'warning', 'msg': 'Wrong login or password'}

        except Exception, e:
            self.logger.error(str(e), exc_info=True)
            return False, {
                'status': 'error',
                'error_type': str(type(e)),
                'error_msg': repr(e)
            }

    def logout(self):
        """
        Logout user from system

        Returns:
            Tuple (Boolean,dict), If boolean is true the dict is None else the dict has structure:
                {'status':'warning','msg','warning message'} or
                {'status':'error','error_type':'Exception','msg':'error message'}
        """
        try:
            # get user from session
            user = self.get_user()

            # check is user is not None
            if user is None:
                # user is NoneType, return true that session already cleared
                return self._clear_session(user)

            # logout user
            status, result = user.logout()

            # if logout has no errors clear the session
            if status:
                self.logger.debug('Logout user')
                return self._clear_session(user)
            else:
                # return error
                self._clear_session()
                return False, result
        except Exception, e:
            self.logger.error(str(e), exc_info=True)
            return False, {
                'status': 'error',
                'error_type': str(type(e)),
                'error_msg': repr(e)
            }
    def check_session(self, *args):
        """"""
        if 'user_id' in session:
           return True, self.USER_MODEL.query.filter(getattr(self.USER_MODEL, self.USER_ID_ATTR) == session['user_id']).first().to_json()
        else:
            return False, None
        
    def reload_session(self, user):
        """
        Reload the user session

        Returns:
            Tuple (True,None)
        """
        # set session login key
        session[self.LOGIN_KEY] = True

        # set user id
        session['user_id'] = getattr(user, self.USER_ID_ATTR)

        self.logger.debug('Reload session for user id: {0}'.format(getattr(user, self.USER_ID_ATTR)))
        return True, None

    def _set_session(self, user):
        """
        Set the user session

        Returns:
            Tuple (True,None)
        """
        # set session login key
        session[self.LOGIN_KEY] = True

        # set user id
        session['user_id'] = getattr(user, self.USER_ID_ATTR)

        self.logger.debug('Set new session for user id: {0}' .format(getattr(user, self.USER_ID_ATTR)))
        return True, None

    def _clear_session(self, user=None):
        """
        Clear all session data

        Returns:
            Tuple (True,None)
        """
        # clear session
        session.clear()

        if user is not None:
            self.logger.debug('Clear session for user: {0}' .format(getattr(user, self.USER_ID_ATTR)))

        return True, None

    @staticmethod
    def set_session_key(key, value):
        """Add custom key, vaule to session

        Args:
            key (str): key to add
            value (str): value of key that will be added to session
        """
        session[key] = value

    @staticmethod
    def get_session_value(key):
        """Return value of key from session dict

        Args:
            key (str): key to return vaule
        """
        return session.get(key, None)

    @staticmethod
    def clear_session_key(key):
        """Clear key session dict

        Args:
            key (str): key to clear
        """
        session.pop(key, None)

    def login_require(self, f):
        """
        Check if user is logged in if not redirect him to login page

        Returns:
            If user is logged in then the wrapped function will be returned if not
            return redirection to url_for registered with register_redirect_url method
        """

        @wraps(f)
        def wrap(*args, **kwargs):
            if self.LOGIN_KEY in session:
                if self.get_session_value(self.LOGIN_KEY):
                    return f(*args, **kwargs)

            if self.REDIRECT_URL is not None:
                flash(u'Proszę zalogować się do systemu', 'danger')
                return redirect(url_for(self.REDIRECT_URL))
            else:
                resp = dict(
                    type='error',
                    title='AuthorizationException',
                    messages=['Brak autoryzacji']
                )
                return self.create_response(json.dumps(resp), 401)
        return wrap

    def get_user(self, only_id=False):
        """Get user model object

        Returns:
            sqlalchemy.db.model: when user is logged in
            None: when user is not logged in or
            user_id: when only_id is True
        """

        if 'user_id' in session:
            if not only_id:
                return self.USER_MODEL.query.filter(
                    getattr(self.USER_MODEL, self.USER_ID_ATTR) == session['user_id']
                ).first()
            else:
                return session.get('user_id', None)
        else:
            return None

    def update_user(self, key, value):
        """"Update user data

        Args:
            key (str): key to change
            value (str): new value
        """
        try:
            user = self.get_user()

            if user is None:
                raise TypeError('User is not authenticated, returned NoneType')

            # set value of key
            setattr(user, key, value)

            # commit changes
            self.DB_INSTANCE.session.commit()

            # return success
            return True, None

        except Exception as e:
            # rollback changes
            self.DB_INSTANCE.session.rollback()

            # log error
            current_app.logger.error(str(e), exc_info=True)

            # return failed status
            return False, {
                'status': 'error',
                'error_type': str(type(e)),
                'error_msg': repr(e)
            }

    @property
    def is_authenticated(self):
        """Check if user is authenticated"""
        # get user
        user = self.get_user()

        # check
        if user is None:
            return False
        else:
            return True

    @staticmethod
    def get_random_password(length=12):
        """
        Generates random string hash

        Args:
            length (int): length of hash that will be generated

        Returns:
            String hash
        """
        # get random string
        random_string = ''.join(random.choice(SIMPLE_CHARS) for i in xrange(length))

        hash_data = sha512()
        hash_data.update(random_string)
        return hash_data.hexdigest()[:length]

    @staticmethod
    def create_response(content, code, content_type='application/json; charset="UTF-8"', cors=None):
        """
        Create response object

        Args:
            content (str(example: dump json)): content of the response
            code (int): http code of the response
            content_type='application/json; charset="UTF-8"' (str): content type of the response

        Returns:
            Flask response object
        """
        resp = make_response(content, code)
        if cors is not None:
            resp.headers['Access-Control-Allow-Origin'] = cors
        resp.headers['Content-Type'] = content_type
        resp.headers['Cache-Control'] = 'no-cache'
        return resp
