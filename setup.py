# -*- coding: utf-8 -*-
"""
Flask-Simple-Session
------------

Flask extension for user login, logout and session manage.
"""
from setuptools import setup


setup(
    name='Flask-Simple-Session',
    version='2.2.5',
    url='',
    license='MIT',
    author='Marcin Wojtysiak',
    author_email='wojtysiak.marcin@gmail.com',
    description='Flask extension for user login, logout and session manage.',
    long_description=__doc__,
    packages=['flask_simple_session'],
    zip_safe=False,
    include_package_data=True,
    platforms='any',
    install_requires=[
        'flask',
        'sqlalchemy'
    ],
    tests_require=[],
    test_suite='test_simple_session',
    classifiers=[
        'Environment :: Web Environment',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Operating System :: OS Independent',
        'Programming Language :: Python',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
        'Topic :: Software Development :: Libraries :: Python Modules'
    ]
)
